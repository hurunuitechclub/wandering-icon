 /*
 * Copyright © 2021 Ryan McCoskrie <work@ryanmccoskrie.me>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
 * associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import QtQuick 2.0
import QtQuick.Controls 2.1 as QQC2
import QtGraphicalEffects 1.0
import QtQuick.Window 2.2
import org.kde.plasma.core 2.0
import org.kde.plasma.wallpapers.image 2.0 as Wallpaper
import org.kde.plasma.core 2.0 as PlasmaCore
 
 Rectangle {
	id: root
	color: wallpaper.configuration.Colour

	property string iconFile: wallpaper.configuration.Icon

	property int xSpeed: wallpaper.configuration.Speed
	property int ySpeed: wallpaper.configuration.Speed
	
	function randomInt(max)
	{
		return Math.floor(Math.random() * max + 1);
	}

	function atHorizontalEdge()
	{
		if(icon.x <= 0 || icon.x >= (Screen.width - icon.width))
			return true;
		else
			return false;
	}

	function atVerticalEdge()
	{
		if(icon.y <= 0 || icon.y >= (Screen.height - icon.height))
			return true;
		else
			return false;
	}

	function updateLocation()
	{
		icon.x = icon.x + xSpeed; icon.y = icon.y + ySpeed
	}

	Image {
		id: icon
		source: iconFile
		x: randomInt(Screen.width - width)
		y: randomInt(Screen.height - height)
	}

	Timer {
		interval: 3; running: true; repeat: true
		onTriggered: {
			if(atHorizontalEdge())
				xSpeed = xSpeed - xSpeed * 2;
			if(atVerticalEdge())
				ySpeed = ySpeed - ySpeed * 2;
			updateLocation();
		}
	}
 }
